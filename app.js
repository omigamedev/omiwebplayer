/// <reference path="gl-matrix.d.ts" />
var canvas = document.getElementById("render");
var gl = canvas.getContext("webgl");
var speed_span = document.getElementById("speed_span");
var alpha_span = document.getElementById("alpha-span");
var beta_span = document.getElementById("beta-span");
var gamma_span = document.getElementById("gamma-span");
var btn_add = document.getElementById("btn-add");
var btn_sub = document.getElementById("btn-sub");
var log_pre = document.getElementById("log_pre");
var play_button = document.getElementById("play-button");
var red = 0.0;
var dir = 1;
var speed = 0;
function updateSpeed(offset) {
    speed += offset;
    speed_span.innerHTML = speed.toFixed(2);
}
function log(msg) {
    log_pre.innerHTML += "<br>" + msg;
}
var NetResourceType;
(function (NetResourceType) {
    NetResourceType[NetResourceType["TEXT"] = 0] = "TEXT";
    NetResourceType[NetResourceType["IMAGE"] = 1] = "IMAGE";
    NetResourceType[NetResourceType["VIDEO"] = 2] = "VIDEO";
})(NetResourceType || (NetResourceType = {}));
;
var ResourcesManager = (function () {
    function ResourcesManager() {
        this.resources = [];
    }
    ResourcesManager.prototype.addResource = function (res) {
        this.resources.push(res);
    };
    ResourcesManager.prototype.load = function () {
        if (this.resources.length == 0) {
            log("finished resource loading");
            this.onFinished();
            return;
        }
        var r = this.resources.pop();
        switch (r.type) {
            case NetResourceType.TEXT:
                this.load_generic(r, "text/plain");
                break;
            case NetResourceType.IMAGE:
                this.load_image(r);
                break;
            case NetResourceType.VIDEO:
                this.load_video(r);
                break;
        }
    };
    ResourcesManager.prototype.load_generic = function (r, mime) {
        log("loading generic " + mime + ": " + r.name);
        var parent = this;
        var req = new XMLHttpRequest();
        req.overrideMimeType(mime);
        req.open("GET", r.name, true);
        req.onreadystatechange = function (ev) {
            if (req.readyState == 4 && req.status == 200) {
                log("complete: " + r.name);
                r.content = req.responseText;
                parent.load();
            }
        };
        req.send(null);
    };
    ResourcesManager.prototype.load_image = function (r) {
        log("loading image: " + r.name);
        var parent = this;
        var image = new Image();
        image.onload = function (ev) {
            log("complete : " + r.name);
            r.content = image;
            parent.load();
        };
        image.src = r.name;
    };
    ResourcesManager.prototype.load_video = function (r) {
        log("loading video: " + r.name);
        var parent = this;
        var video = document.createElement("video");
        video.onloadeddata = function (ev) {
            log("complete : " + r.name);
            r.content = video;
            parent.load();
        };
        video.src = r.name;
    };
    return ResourcesManager;
})();
var Sphere = (function () {
    function Sphere() {
    }
    Sphere.prototype.create = function (latitudeBands, longitudeBands, radius) {
        var vertexPositionData = [];
        var normalData = [];
        var textureCoordData = [];
        var indexData = [];
        for (var latNumber = 0; latNumber <= latitudeBands; latNumber++) {
            var theta = latNumber * Math.PI / latitudeBands;
            var sinTheta = Math.sin(theta);
            var cosTheta = Math.cos(theta);
            for (var longNumber = 0; longNumber <= longitudeBands; longNumber++) {
                var phi = longNumber * 2 * Math.PI / longitudeBands;
                var sinPhi = Math.sin(phi);
                var cosPhi = Math.cos(phi);
                var x = cosPhi * sinTheta;
                var y = cosTheta;
                var z = sinPhi * sinTheta;
                var u = 1 - (longNumber / longitudeBands);
                var v = 1 - (latNumber / latitudeBands);
                normalData.push(x);
                normalData.push(y);
                normalData.push(z);
                textureCoordData.push(u);
                textureCoordData.push(v);
                vertexPositionData.push(radius * x);
                vertexPositionData.push(radius * y);
                vertexPositionData.push(radius * z);
            }
        }
        for (var latNumber = 0; latNumber < latitudeBands; latNumber++) {
            for (var longNumber = 0; longNumber < longitudeBands; longNumber++) {
                var first = (latNumber * (longitudeBands + 1)) + longNumber;
                var second = first + longitudeBands + 1;
                indexData.push(first);
                indexData.push(second);
                indexData.push(first + 1);
                indexData.push(second);
                indexData.push(second + 1);
                indexData.push(first + 1);
            }
        }
        this.vb = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vb);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexPositionData), gl.STATIC_DRAW);
        this.tb = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.tb);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoordData), gl.STATIC_DRAW);
        this.ib = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ib);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexData), gl.STATIC_DRAW);
        this.count = indexData.length;
    };
    Sphere.prototype.draw = function () {
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vb);
        gl.vertexAttribPointer(pos_attrib, 3, gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.tb);
        gl.vertexAttribPointer(tex_attrib, 2, gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ib);
        gl.drawElements(gl.TRIANGLES, this.count, gl.UNSIGNED_SHORT, 0);
    };
    return Sphere;
})();
// Compile single shader
function compileShader(type, code) {
    var shader = gl.createShader(type);
    gl.shaderSource(shader, code);
    gl.compileShader(shader);
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        var err = gl.getShaderInfoLog(shader);
        log("Shader: " + err);
    }
    return shader;
}
function createProgram(vs, ps) {
    var prog = gl.createProgram();
    var shaderv = compileShader(gl.VERTEX_SHADER, vs);
    var shaderp = compileShader(gl.FRAGMENT_SHADER, ps);
    gl.attachShader(prog, shaderv);
    gl.attachShader(prog, shaderp);
    gl.linkProgram(prog);
    if (!gl.getProgramParameter(prog, gl.LINK_STATUS)) {
        var err = gl.getProgramInfoLog(prog);
        log("Program: " + err);
    }
    return prog;
}
// Initialize elements
log("ui init");
btn_add.onclick = function (ev) { updateSpeed(0.01); };
btn_sub.onclick = function (ev) { updateSpeed(-0.01); };
play_button.onclick = function (ev) {
    player.play();
};
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
updateSpeed(0.01);
var simple_vertex_glsl = { name: "simple-vertex.glsl", type: NetResourceType.TEXT };
var simple_fragment_glsl = { name: "simple-fragment.glsl", type: NetResourceType.TEXT };
var image_png = { name: "wood5.png", type: NetResourceType.IMAGE };
var video_mp4 = { name: "star-wars.mp4", type: NetResourceType.VIDEO };
var rm = new ResourcesManager();
rm.addResource(simple_vertex_glsl);
rm.addResource(simple_fragment_glsl);
rm.addResource(image_png);
rm.addResource(video_mp4);
rm.onFinished = initApplication;
rm.load();
log("load shaders");
//loadShaders(initApplication);
var vbuffer;
var tbuffer;
var ibuffer;
var simpleProgram;
var pos_attrib;
var tex_attrib;
var modelview_uniform;
var texmat_uniform;
var proj_uniform;
var sampler_uniform;
var texture;
var player;
var sphere = new Sphere();
var yaw = 0;
var pitch = 0;
function initApplication() {
    var vertex_code = simple_vertex_glsl.content;
    var fragment_code = simple_fragment_glsl.content;
    var image = image_png.content;
    player = video_mp4.content;
    player.onplay = function () { play_button.src = "control_pause_blue.png"; };
    player.onended = function () { play_button.src = "control_play_blue.png"; };
    player.onpause = function () { play_button.src = "control_play_blue.png"; };
    // Init GL
    log("buffers init");
    vbuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([0, 0, 1, 0, 1, 1, 0, 1]), gl.STATIC_DRAW);
    tbuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, tbuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([0, 0, 1, 0, 1, 1, 0, 1]), gl.STATIC_DRAW);
    ibuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array([0, 1, 2, 0, 2, 3]), gl.STATIC_DRAW);
    log("shaders init");
    simpleProgram = createProgram(vertex_code, fragment_code);
    pos_attrib = gl.getAttribLocation(simpleProgram, "pos");
    tex_attrib = gl.getAttribLocation(simpleProgram, "tex");
    modelview_uniform = gl.getUniformLocation(simpleProgram, "modelview");
    texmat_uniform = gl.getUniformLocation(simpleProgram, "texmat");
    proj_uniform = gl.getUniformLocation(simpleProgram, "proj");
    sampler_uniform = gl.getUniformLocation(simpleProgram, "sampler");
    texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
    // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
    // gl.generateMipmap(gl.TEXTURE_2D);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.bindTexture(gl.TEXTURE_2D, null);
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.enableVertexAttribArray(pos_attrib);
    gl.enableVertexAttribArray(tex_attrib);
    gl.useProgram(simpleProgram);
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.uniform1i(sampler_uniform, 0);
    sphere.create(64, 64, 1);
    sphere.pos_attrib = pos_attrib;
    sphere.tex_attrib = tex_attrib;
    player.setAttribute("webkit-playsinline", "");
    player.setAttribute("playsinline", "");
    player.muted = true;
    player.play();
    log("start rendering");
    RenderLoop();
    log("touch to play the video");
    window.ontouchstart = function () { player.play(); };
    // window.ondeviceorientation = function(ev:DeviceOrientationEvent)
    // {
    //     if (ev.alpha == undefined)
    //         return;
    //     alpha_span.innerHTML = ev.alpha.toFixed(0);
    //     beta_span.innerHTML = ev.beta.toFixed(0);
    //     gamma_span.innerHTML = ev.gamma.toFixed(0);
    //     yaw = ev.alpha;
    //     pitch = ev.beta;
    // }
    var touch_start = null;
    var touch_delta = vec2.create();
    var old_yaw = yaw;
    var old_pitch = pitch;
    // Handle touch events
    canvas.ontouchstart = function (ev) {
        var t = ev.touches.item(0);
        touch_start = vec2.fromValues(t.clientX, t.clientY);
        old_yaw = yaw;
        old_pitch = pitch;
    };
    canvas.ontouchmove = function (ev) {
        if (touch_start != null) {
            var t = ev.touches.item(0);
            var p = vec2.fromValues(t.clientX, t.clientY);
            vec2.sub(touch_delta, p, touch_start);
            yaw = old_yaw + touch_delta[0];
            pitch = old_pitch + touch_delta[1];
        }
    };
    canvas.ontouchend = function (ev) {
        touch_start = null;
    };
    // Handle mouse events
    canvas.onmousedown = function (ev) {
        touch_start = vec2.fromValues(ev.clientX, ev.clientY);
        old_yaw = yaw;
        old_pitch = pitch;
    };
    canvas.onmousemove = function (ev) {
        if (touch_start != null) {
            var p = vec2.fromValues(ev.clientX, ev.clientY);
            vec2.sub(touch_delta, p, touch_start);
            yaw = old_yaw + touch_delta[0];
            pitch = old_pitch + touch_delta[1];
        }
    };
    canvas.onmouseup = function (ev) {
        touch_start = null;
    };
    // Fullscreen mode
    canvas.ondblclick = function (ev) {
        //document.documentElement.webkitRequestFullscreen();
        if (canvas.webkitRequestFullScreen) {
            canvas.webkitRequestFullScreen();
        }
        else {
            canvas.mozRequestFullScreen();
        }
    };
    document.onfullscreenchange = function (ev) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        gl.viewport(0, 0, canvas.width, canvas.height);
    };
    document.body.ontouchmove = function (ev) {
        ev.preventDefault();
    };
}
var theta = 0;
function RenderLoop() {
    theta += 1;
    red += speed * dir;
    if (red > 1 || red < 0)
        dir *= -1;
    gl.clearColor(0, 1, 0, 1);
    gl.clear(gl.COLOR_BUFFER_BIT);
    // upload the video frame
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, player);
    var texmat = mat3.create();
    mat3.scale(texmat, texmat, vec2.fromValues(1, 1));
    gl.uniformMatrix3fv(texmat_uniform, false, texmat);
    var proj = mat4.create();
    mat4.perspective(proj, glMatrix.toRadian(85), canvas.width / canvas.height, .1, 100);
    gl.uniformMatrix4fv(proj_uniform, false, proj);
    var modelview = mat4.create();
    // Draw the square
    //     mat4.identity(modelview);
    //     mat4.scale(modelview, modelview, vec3.fromValues(.5, .5, 1));
    //     mat4.translate(modelview, modelview, vec3.fromValues(0.5, 0.5, .1));
    //     gl.uniformMatrix4fv(modelview_uniform, false, modelview);
    //
    //     gl.bindBuffer(gl.ARRAY_BUFFER, vbuffer);
    //     gl.vertexAttribPointer(pos_attrib, 2, gl.FLOAT, false, 0, 0);
    //     gl.bindBuffer(gl.ARRAY_BUFFER, tbuffer);
    //     gl.vertexAttribPointer(tex_attrib, 2, gl.FLOAT, false, 0, 0);
    //     gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibuffer);
    //     gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);
    // Draw the sphere
    var rot_scale = 0.1;
    mat4.identity(modelview);
    mat4.rotateZ(modelview, modelview, glMatrix.toRadian(180));
    mat4.rotateX(modelview, modelview, glMatrix.toRadian(pitch * rot_scale));
    mat4.rotateY(modelview, modelview, glMatrix.toRadian(yaw * rot_scale - 90));
    gl.uniformMatrix4fv(modelview_uniform, false, modelview);
    sphere.draw();
    window.requestAnimationFrame(RenderLoop);
}
