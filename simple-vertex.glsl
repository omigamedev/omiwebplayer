attribute vec3 pos;
attribute vec2 tex;
uniform mat4 modelview;
uniform mat4 proj;
uniform mat3 texmat;
varying highp vec2 tc;
void main()
{
    gl_Position = proj * modelview * vec4(pos, 1);
    tc = (texmat * vec3(tex, 1)).st;
}
