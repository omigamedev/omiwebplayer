uniform sampler2D sampler;
varying highp vec2 tc;
void main()
{
    mediump vec4 c = texture2D(sampler, tc);
    gl_FragColor = c; 
}
