**WebPlayer360 project**
This is a draft documentation.

**NodeJS static web server**
NodeJS server it's used in order to test resource loading with XMLHTTPRequest which supports only http:// protocol and not the file://.
Link: http://stackoverflow.com/questions/6084360/using-node-js-as-a-simple-web-server 

**krpano web player**
This is an example of how to use video texture in WebGL.
Link: http://krpano.com/ios/bugs/ios8-webgl-video/

**gl-matrix library**
High performace js math library with handful types for 3D graphics, especialli with WebGL.
Typescript definitions: https://github.com/chuntaro/gl-matrix.d.ts
Javascript library: https://github.com/toji/gl-matrix/blob/master/dist/gl-matrix.js
Homepage: http://glmatrix.net